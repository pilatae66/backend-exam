<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    public function login(Request $request)
    {
        $login_data = $request->validate([
            'email' => 'required|email',
            'password' => 'required|string|min:7'
        ]);

        if(!auth()->attempt($login_data)) return response(['message' => 'The given data was invalid', 'errors' => ['email' => ['These credentials do not match our records.']]]);

        $token = auth()->user()->createToken('MyApp');

        return response(['token' => $token->accessToken, 'token_type' => 'bearer', 'expires_at' => $token->token->expires_at], 200);
    }

    public function register(Request $request)
    {
        $validated_data = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:7|confirmed'
        ]);

        $validated_data['password'] = bcrypt($validated_data['password']);

        $user = User::create($validated_data);
        $token = $user->createToken('MyApp');

        return response($user, 201);
    }

    public function logout()
    {
        //nothing to do here
    }
}
