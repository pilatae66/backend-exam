<?php

namespace App\Http\Controllers\API;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CommentResource::collection(Comment::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $validated_data = $request->validate([
            'body' => 'required|string|max:255'
        ]);

        $post = Post::where('slug', $slug)->firstOrFail();
        $comment = new Comment;
        $comment->body = $validated_data['body'];
        $comment->creator_id = auth()->user()->id;
        $comments = $post->comments()->save($comment);

        return new CommentResource($comments);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug, $commentId)
    {

        $validated_data = $request->validate([
            'body' => 'string|max:255'
        ]);

        $post = Post::where('slug', $slug)->firstOrFail();
        $comment = Comment::findOrFail($commentId);
        $comment->body = $validated_data['body'];
        $comment->creator_id = auth()->user()->id;
        $comments = $post->comments()->save($comment);

        return new CommentResource($comments);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug, $commentId)
    {
        Comment::findOrFail($commentId)->delete();
        return response(['status' => 'record deleted successfully'], 200);
    }
}
