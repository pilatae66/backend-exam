<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\PostIndexResource;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PostIndexResource(Post::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated_data = $request->validate([
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:255',
            'image' => 'required|string'
        ]);

        $validated_data['user_id'] = auth()->user()->id;
        $validated_data['slug'] = Str::slug($validated_data['title']);
        $post = Post::create($validated_data);

        return response(new PostResource($post), 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $validated_data = $request->validate([
            'title' => 'string|max:255',
            'content' => 'string|max:255'
        ]);

        $updated_post = Post::where('slug', $slug)->firstOrFail();

        $validated_data['slug'] = Str::slug($validated_data['title']);
        $updated_post->update($validated_data);

        return response(new PostResource($updated_post), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        Post::where('slug', $slug)->firstOrFail()->delete();

        return response(['status' => 'record deleted successfully'], 200);
    }
}
