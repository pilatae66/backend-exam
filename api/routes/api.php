<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'API\AuthenticationController@register');
Route::post('/login', 'API\AuthenticationController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/logout', 'API\AuthenticationController@logout');
    Route::post('/posts', 'API\PostController@store');
    Route::patch('posts/{slug}', 'API\PostController@update');
    Route::delete('posts/{slug}', 'API\PostController@destroy');
    Route::post('posts/{slug}/comments', 'API\CommentController@store');
    Route::patch('posts/{slug}/comments/{commentId}', 'API\CommentController@update');
    Route::delete('posts/{slug}/comments/{commentId}', 'API\CommentController@destroy');
});

Route::get('/posts', 'API\PostController@index');
Route::get('posts/{slug}', 'API\PostController@show');
Route::get('posts/{slug}/comments', 'API\CommentController@index');
